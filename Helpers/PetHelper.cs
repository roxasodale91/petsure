using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using petsure.Models;

namespace petsure.Helpers
{
    public class PetHelper
    {
        private List<Pets> petList = new List<Pets>();

        private List<string> names = new List<string>();
        public PetHelper()
        {
            this.names = new List<string>(new[] {"Rover","Fido","Pixie"});

            foreach(var name in names)
            {
                petList.Add(new Pets{
                    id = Guid.NewGuid().ToString(),
                    name = name
                });
            }
        }
        public void Add(Pets pet)
        {
            this.petList.Add(pet);
        }

        public List<Pets> GetPets()
        {
            return this.petList;
        }

        public bool InsertPet(Pets pet)
        {
            try{
                this.petList.Add(pet);
            }
            catch(SystemException ex)
            {
                var mes = ex.Message.ToString();

                return false;
            }

            return true;
        }

        public bool EditPet(Pets pet)
        {

            var existingPet = petList.Where(x=>x.id == pet.id).FirstOrDefault();
            try{
                existingPet.id = pet.id;
                existingPet.name = pet.name;
            }
            catch(SystemException ex)
            {
                var mes = ex.Message.ToString();
                return false;
            }

            return true;
        }

        public bool DeletePet(Pets pet)
        {
            try{
                petList.Remove(pet);
            }
            catch(SystemException ex)
            {
                var mes = ex.Message.ToString();
                return false;
            }

            return true;
        }
    }
}