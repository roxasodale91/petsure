using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using petsure.Models;
using petsure.Helpers;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;

namespace petsure.Controllers
{

    [Produces("application/json")]
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]/[action]")]
    public class PetController : ControllerBase
    {
        PetHelper ph = new PetHelper();

        private IHostingEnvironment _hostingEnvironment;

        public PetController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet]
        [Microsoft.AspNetCore.Mvc.ActionName("GetPets")]
        public JsonResult GetPets()
        {
            return new JsonResult(this.ph.GetPets());
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.ActionName("AddPets")]
        public bool InsertPet([Microsoft.AspNetCore.Mvc.FromBody]Pets pet)
        {
            return  this.ph.InsertPet(pet);
        }

        public bool EditPet(Pets pet)
        {
            return this.ph.EditPet(pet);
        }

        public bool DeletePet(Pets pet)
        {
            return this.ph.DeletePet(pet);
        }


        [Microsoft.AspNetCore.Mvc.HttpPost, DisableRequestSizeLimit]
        [Microsoft.AspNetCore.Mvc.ActionName("UploadFile")]
        public ActionResult UploadFile()
        {
            try
            {
                var file = Request.Form.Files[0];
                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                return new JsonResult("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return new JsonResult("Upload Failed: " + ex.Message);
            }
        }
    }
}
