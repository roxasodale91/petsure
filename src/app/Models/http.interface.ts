import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { all } from 'q';


export interface IHttpService<T> {
  dataSubject: BehaviorSubject<T[]>;
  apiURL: string;
  dataStore: {
    dataarray: T[];
  };

  loadAll();

  addData(object: T);
}

export class HttpService<T> implements IHttpService<T> {
  dataSubject: BehaviorSubject<T[]>;
  apiURL: string;
  dataStore: { dataarray: T[]; };
  http: HttpClient;

  constructor(private defineHttp: HttpClient) {
    this.dataStore = {dataarray: []};
    this.dataSubject = new BehaviorSubject(this.dataStore.dataarray);
  }
  get all() {
    return this.dataSubject.asObservable();
  }
  loadAll() {
    this.http.get<T[]>(this.apiURL + 'GetPets')
    .subscribe(data => {
      this.dataStore.dataarray = data;
      this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
    });
  }
  addData(object: T) {
    this.dataStore.dataarray.push(object);
    this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);

    this.http.post(this.apiURL + 'AddPets/', object)
    .subscribe(
      res => {
        console.log(res);
        // this.dataStore.dataarray.push(object);
        // this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
      },
      err => {
        console.log('Error occured');
      }
    );
  }

  deleteData(object: T) {
    const index = this.dataStore.dataarray.indexOf(object);
    this.dataStore.dataarray.splice(index, 1);
    this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
  }

  editData(object: T) {
    this.dataStore.dataarray.push(object);
    this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
    this.http.put(this.apiURL + 'Edit/', object)
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log('Error occured');
      }
    );
  }

}
