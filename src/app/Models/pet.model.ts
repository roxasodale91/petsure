export class Pets {
  id: string;
  name: string;
}

export class PetsProps implements Pets {
  id: string;
  name: string;

  constructor() {
    this.id = 'id';
    this.name = 'name';
  }
}
