import { HttpHeaders, HttpParams } from '@angular/common/http';
import { ExtensionService } from './Extensions/format.extensions';
import { FormInterface } from './Models/form.interface';
import { PetserviceService } from './Services/petservice.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { Pets, PetsProps } from './Models/pet.model';
import { Observable } from '../../node_modules/rxjs';
import { FileValidator } from '../../node_modules/ngx-material-file-input';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _service: PetserviceService) {
    this._service.loadAll();
  }

}
