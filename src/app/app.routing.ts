import { AppComponent } from './app.component';
import { PetsComponent } from './Components/pets/pets.component';
import { ClaimsComponent } from './Components/claims/claims.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';


const router: Routes = [
  {path: 'claims', component: ClaimsComponent},
  {path: 'pets', component: PetsComponent},
  {path: '**', component: ClaimsComponent}
];


export const routes: ModuleWithProviders  = RouterModule.forRoot(router);
export const appRoutes = RouterModule.forChild(router);
