import { HttpService } from './../Models/http.interface';
import { Pets } from '../Models/pet.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PetserviceService extends HttpService<Pets> {

  constructor(defineHttp: HttpClient) {
    super(defineHttp);
    this.http = defineHttp;
    this.apiURL = 'https://localhost:5001/api/Pet/';
  }

}
