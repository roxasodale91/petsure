/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PetserviceService } from './petservice.service';

describe('Service: Petservice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PetserviceService]
    });
  });

  it('should ...', inject([PetserviceService], (service: PetserviceService) => {
    expect(service).toBeTruthy();
  }));
});
