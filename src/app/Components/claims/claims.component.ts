import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { PetsProps } from './../../Models/pet.model';
import { ExtensionService } from './../../Extensions/format.extensions';
import { PetserviceService } from './../../Services/petservice.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormInterface } from './../../Models/form.interface';
import { Component, OnInit } from '@angular/core';
import { Pets } from '../../Models/pet.model';

@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.css']
})
export class ClaimsComponent implements OnInit, FormInterface<Pets> {

  form = new FormGroup({});
  model = new Pets;
  testmodel: Pets = new Pets;
  properties = new PetsProps;
  viewmodel: any;
  options: FormlyFormOptions;
  values: Pets[];
  files: any[];

  pets: Observable<Pets[]>;
  public progress: number;
  public message: string;
  fields: FormlyFieldConfig[] = [
    {
      key: this.properties.id.toString(),
      type: 'select',
      templateOptions: {
        label: 'Make a claim for your pet',
        placeholder: 'Placeholder',
        description: 'Please select a Pet',
        required: true,
        options: this._extension.formatted,
      },
    }
  ];


  public uploadEvent($event: any) {
    console.log('from client' + JSON.stringify($event));
  }

  onSubmit(form: FormGroup) {}

  submit() {
    // this.testmodel.id = 'Test Input';
    // this.testmodel.name = 'Atlas';
    // this._service.addData(this.testmodel);
  }

  upload(files) {
    if (files.length === 0) {
      return;
    }

    console.log(files);
    const formData = new FormData();

    for (const file of files) {
       formData.append(file.name, file);
    }
    console.log(formData);
    const uploadReq = new HttpRequest('POST', `https://localhost:5001/api/Pet/UploadFile`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      }　else if (event.type === HttpEventType.Response) {
        this.message = event.body.toString();
           }
    });
  }
  ngOnInit(): void {
    this._extension.format(this.pets);
    this._service.all
     .subscribe((data: Pets[]) => {
       this.values = data;
      });
  }


  constructor(private _service: PetserviceService, private _extension: ExtensionService,
    private _fb: FormBuilder, private http: HttpClient) {
    this.pets = this._service.all;
  }

}
