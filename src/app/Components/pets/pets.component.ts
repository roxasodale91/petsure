import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { FormInterface } from './../../Models/form.interface';
import { PetserviceService } from './../../Services/petservice.service';
import { Component, OnInit } from '@angular/core';
import { Pets, PetsProps } from '../../Models/pet.model';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent implements OnInit, FormInterface<Pets> {

  form = new FormGroup({});
  model = new Pets;
  properties = new PetsProps;
  viewmodel: any;
  options = {};
  values: Pets[];
  fields: FormlyFieldConfig[] = [
    {
      key: this.properties.name.toString(),
      type: 'input',
      templateOptions: {
        label: 'Name of Pet',
        placeholder: 'Placeholder',
        description: 'Please enter a Pet Name',
        required: true,
      },
    }
  ];
  // Mat Table
  displayedColumns: string[] = ['id', 'name'];

  submit() {
    this.model.id = Guid.create().toString();
    this._service.addData(this.model);
    this.model = new Pets;
  }

  constructor(private _service: PetserviceService) {
  }

  ngOnInit() {
    this._service.all
     .subscribe((data: Pets[]) => {
       this.values = data;
      });
  }

}
