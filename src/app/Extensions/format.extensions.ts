import { Observable, BehaviorSubject } from 'rxjs';

export class ExtensionService {


  returnFiltered: any[];
  private returnSubject: BehaviorSubject<any[]>;


  constructor() {
    this.returnFiltered = [];
    this.returnSubject = new BehaviorSubject(this.returnFiltered);
  }

  format(entity: Observable<any[]>) {
    entity.subscribe((data: any[]) => {

      this.returnFiltered = [];
      for (let i = 0; data.length > i; i++) {
        this.returnFiltered.push({
          value: data[i].id.toString(),
          label: data[i].name.toString()
        });
      }
      this.returnSubject.next(this.returnFiltered);
    });
  }

  get formatted() {
    return this.returnSubject.asObservable();
  }
}
