import { RouterModule } from '@angular/router';
import { ClaimsComponent } from './Components/claims/claims.component';
import { FormlyFieldFile, FileValueAccessor } from './file-value.accessor';
import { MatIconModule } from '@angular/material/icon';
import { HttpsRequestInterceptor, InterceptorModule } from './HttpsRequestInterceptor/HttpsRequestInterceptor.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatSelectModule,
  MatInputModule, MatFormFieldModule, MatSliderModule, MatToolbarModule,
  MatCardModule,
  MatMenuModule,
  MatTableModule} from '@angular/material';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyMaterialModule} from '@ngx-formly/material';
import { PetserviceService } from './Services/petservice.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { ExtensionService } from './Extensions/format.extensions';
import { RepeatTypeComponent } from './repeat-component';
import { PetsComponent } from './Components/pets/pets.component';
import {routes, appRoutes} from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    RepeatTypeComponent,
    FileValueAccessor,
    FormlyFieldFile,
    ClaimsComponent,
    PetsComponent
  ],
  imports: [
    routes,
    appRoutes,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatSliderModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({types: [
      { name: 'repeat', component: RepeatTypeComponent },
      { name: 'file', component: FormlyFieldFile },
      ],
    }),
    FormlyMaterialModule,
    HttpClientModule,
    InterceptorModule,
    MatIconModule,
    MatTableModule

  ],
  providers: [PetserviceService,
  ExtensionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
