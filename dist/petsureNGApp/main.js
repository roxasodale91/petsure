(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Components/claims/claims.component.css":
/*!********************************************************!*\
  !*** ./src/app/Components/claims/claims.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Components/claims/claims.component.html":
/*!*********************************************************!*\
  !*** ./src/app/Components/claims/claims.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>\n  Make a claim\n</h3>\n<form [formGroup]=\"form\">\n  <formly-form [model]=\"model\" [fields]=\"fields\" [options]=\"options\" [form]=\"form\">\n  </formly-form>\n</form>\n\n\n<div style=\"padding-top:20vh;\">\n  <form [formGroup]=\"form\" (ngSubmit)=\"submit()\">\n    <formly-form [model]=\"fileModel\" [fields]=\"repeat\" [form]=\"form\">\n    </formly-form>\n  </form>\n</div>\n\n\n<button type=\"submit\" class=\"btn btn-primary submit-button\" (click)=\"submit()\">Submit</button>\n"

/***/ }),

/***/ "./src/app/Components/claims/claims.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/Components/claims/claims.component.ts ***!
  \*******************************************************/
/*! exports provided: ClaimsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClaimsComponent", function() { return ClaimsComponent; });
/* harmony import */ var _Models_pet_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../Models/pet.model */ "./src/app/Models/pet.model.ts");
/* harmony import */ var _Extensions_format_extensions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../Extensions/format.extensions */ "./src/app/Extensions/format.extensions.ts");
/* harmony import */ var _Services_petservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../Services/petservice.service */ "./src/app/Services/petservice.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ClaimsComponent = /** @class */ (function () {
    function ClaimsComponent(_service, _extension, _fb) {
        this._service = _service;
        this._extension = _extension;
        this._fb = _fb;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({});
        this.model = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_0__["Pets"];
        this.testmodel = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_0__["Pets"];
        this.properties = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_0__["PetsProps"];
        this.fileModel = {
            files: [{}],
        };
        this.fields = [
            {
                key: this.properties.id.toString(),
                type: 'select',
                templateOptions: {
                    label: 'Make a claim for your pet',
                    placeholder: 'Placeholder',
                    description: 'Please select a Pet',
                    required: true,
                    options: this._extension.formatted,
                },
            }
        ];
        this.repeat = [
            {
                key: 'files',
                type: 'repeat',
                fieldArray: {
                    fieldGroup: [
                        {
                            key: this.properties.name.toString(),
                            type: 'file'
                        }
                    ],
                },
            },
        ];
        this.pets = this._service.all;
    }
    ClaimsComponent.prototype.uploadEvent = function ($event) {
        console.log('from client' + JSON.stringify($event));
    };
    ClaimsComponent.prototype.onSubmit = function (form) { };
    ClaimsComponent.prototype.submit = function () {
        // this.testmodel.id = 'Test Input';
        // this.testmodel.name = 'Atlas';
        // this._service.addData(this.testmodel);
        console.log(this.fileModel);
    };
    ClaimsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._extension.format(this.pets);
        this._service.all
            .subscribe(function (data) {
            _this.values = data;
        });
    };
    ClaimsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
            selector: 'app-claims',
            template: __webpack_require__(/*! ./claims.component.html */ "./src/app/Components/claims/claims.component.html"),
            styles: [__webpack_require__(/*! ./claims.component.css */ "./src/app/Components/claims/claims.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_petservice_service__WEBPACK_IMPORTED_MODULE_2__["PetserviceService"], _Extensions_format_extensions__WEBPACK_IMPORTED_MODULE_1__["ExtensionService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], ClaimsComponent);
    return ClaimsComponent;
}());



/***/ }),

/***/ "./src/app/Components/pets/pets.component.css":
/*!****************************************************!*\
  !*** ./src/app/Components/pets/pets.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/Components/pets/pets.component.html":
/*!*****************************************************!*\
  !*** ./src/app/Components/pets/pets.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"w-100\">\n  <thead>\n    <tr>\n      <th>Id</th>\n      <th>Name</th>\n    </tr>\n  </thead>\n  <tbody>\n      <tr *ngFor=\"let pet of values\">\n        <td>\n          {{ pet.id}}\n        </td>\n        <td>\n            {{ pet.name}}\n          </td>\n      </tr>\n  </tbody>\n</table>\n\n<div>\n\n<div>\n    <form [formGroup]=\"form\" (ngSubmit)=\"submit()\">\n      <formly-form [model]=\"model\" [fields]=\"fields\" [options]=\"options\" [form]=\"form\">\n          <button type=\"submit\" class=\"btn btn-primary submit-button\" [disabled]=\"!form.valid\" >Submit</button>\n      </formly-form>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/Components/pets/pets.component.ts":
/*!***************************************************!*\
  !*** ./src/app/Components/pets/pets.component.ts ***!
  \***************************************************/
/*! exports provided: PetsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetsComponent", function() { return PetsComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Services_petservice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../Services/petservice.service */ "./src/app/Services/petservice.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Models_pet_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Models/pet.model */ "./src/app/Models/pet.model.ts");
/* harmony import */ var guid_typescript__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! guid-typescript */ "./node_modules/guid-typescript/dist/guid.js");
/* harmony import */ var guid_typescript__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(guid_typescript__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PetsComponent = /** @class */ (function () {
    function PetsComponent(_service) {
        this._service = _service;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroup"]({});
        this.model = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_3__["Pets"];
        this.properties = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_3__["PetsProps"];
        this.options = {};
        this.fields = [
            {
                key: this.properties.name.toString(),
                type: 'input',
                templateOptions: {
                    label: 'Name of Pet',
                    placeholder: 'Placeholder',
                    description: 'Please enter a Pet Name',
                    required: true,
                },
            }
        ];
        // Mat Table
        this.displayedColumns = ['id', 'name'];
    }
    PetsComponent.prototype.submit = function () {
        this.model.id = guid_typescript__WEBPACK_IMPORTED_MODULE_4__["Guid"].create().toString();
        this._service.addData(this.model);
        this.model = new _Models_pet_model__WEBPACK_IMPORTED_MODULE_3__["Pets"];
    };
    PetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._service.all
            .subscribe(function (data) {
            _this.values = data;
        });
    };
    PetsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-pets',
            template: __webpack_require__(/*! ./pets.component.html */ "./src/app/Components/pets/pets.component.html"),
            styles: [__webpack_require__(/*! ./pets.component.css */ "./src/app/Components/pets/pets.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_petservice_service__WEBPACK_IMPORTED_MODULE_1__["PetserviceService"]])
    ], PetsComponent);
    return PetsComponent;
}());



/***/ }),

/***/ "./src/app/Extensions/format.extensions.ts":
/*!*************************************************!*\
  !*** ./src/app/Extensions/format.extensions.ts ***!
  \*************************************************/
/*! exports provided: ExtensionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtensionService", function() { return ExtensionService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

var ExtensionService = /** @class */ (function () {
    function ExtensionService() {
        this.returnFiltered = [];
        this.returnSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](this.returnFiltered);
    }
    ExtensionService.prototype.format = function (entity) {
        var _this = this;
        entity.subscribe(function (data) {
            _this.returnFiltered = [];
            for (var i = 0; data.length > i; i++) {
                _this.returnFiltered.push({
                    value: data[i].id.toString(),
                    label: data[i].name.toString()
                });
            }
            _this.returnSubject.next(_this.returnFiltered);
        });
    };
    Object.defineProperty(ExtensionService.prototype, "formatted", {
        get: function () {
            return this.returnSubject.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    return ExtensionService;
}());



/***/ }),

/***/ "./src/app/HttpsRequestInterceptor/HttpsRequestInterceptor.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/HttpsRequestInterceptor/HttpsRequestInterceptor.module.ts ***!
  \***************************************************************************/
/*! exports provided: HttpsRequestInterceptor, InterceptorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpsRequestInterceptor", function() { return HttpsRequestInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorModule", function() { return InterceptorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var HttpsRequestInterceptor = /** @class */ (function () {
    function HttpsRequestInterceptor() {
    }
    HttpsRequestInterceptor.prototype.intercept = function (req, next) {
        var dupReq = req.clone({ headers: req.headers.set('Access-Control-Allow-Origin', 'http://localhost:5001') });
        return next.handle(dupReq);
    };
    HttpsRequestInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], HttpsRequestInterceptor);
    return HttpsRequestInterceptor;
}());

var InterceptorModule = /** @class */ (function () {
    function InterceptorModule() {
    }
    InterceptorModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HTTP_INTERCEPTORS"], useClass: HttpsRequestInterceptor, multi: true }
            ]
        })
    ], InterceptorModule);
    return InterceptorModule;
}());



/***/ }),

/***/ "./src/app/Models/http.interface.ts":
/*!******************************************!*\
  !*** ./src/app/Models/http.interface.ts ***!
  \******************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

var HttpService = /** @class */ (function () {
    function HttpService(defineHttp) {
        this.defineHttp = defineHttp;
        this.dataStore = { dataarray: [] };
        this.dataSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](this.dataStore.dataarray);
    }
    Object.defineProperty(HttpService.prototype, "all", {
        get: function () {
            return this.dataSubject.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    HttpService.prototype.loadAll = function () {
        var _this = this;
        this.http.get(this.apiURL + 'GetPets')
            .subscribe(function (data) {
            _this.dataStore.dataarray = data;
            _this.dataSubject.next(Object.assign({}, _this.dataStore).dataarray);
        });
    };
    HttpService.prototype.addData = function (object) {
        this.dataStore.dataarray.push(object);
        this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
        // this.http.post(this.apiURL + 'AddPets/', object)
        // .subscribe(
        //   res => {
        //     console.log(res);
        //     // this.dataStore.dataarray.push(object);
        //     // this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
        //   },
        //   err => {
        //     console.log('Error occured');
        //   }
        // );
    };
    HttpService.prototype.deleteData = function (object) {
        var index = this.dataStore.dataarray.indexOf(object);
        this.dataStore.dataarray.splice(index, 1);
        this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
    };
    HttpService.prototype.editData = function (object) {
        this.dataStore.dataarray.push(object);
        this.dataSubject.next(Object.assign({}, this.dataStore).dataarray);
        this.http.put(this.apiURL + 'Edit/', object)
            .subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log('Error occured');
        });
    };
    return HttpService;
}());



/***/ }),

/***/ "./src/app/Models/pet.model.ts":
/*!*************************************!*\
  !*** ./src/app/Models/pet.model.ts ***!
  \*************************************/
/*! exports provided: Pets, PetsProps */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pets", function() { return Pets; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetsProps", function() { return PetsProps; });
var Pets = /** @class */ (function () {
    function Pets() {
    }
    return Pets;
}());

var PetsProps = /** @class */ (function () {
    function PetsProps() {
        this.id = 'id';
        this.name = 'name';
    }
    return PetsProps;
}());



/***/ }),

/***/ "./src/app/Services/petservice.service.ts":
/*!************************************************!*\
  !*** ./src/app/Services/petservice.service.ts ***!
  \************************************************/
/*! exports provided: PetserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PetserviceService", function() { return PetserviceService; });
/* harmony import */ var _Models_http_interface__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../Models/http.interface */ "./src/app/Models/http.interface.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PetserviceService = /** @class */ (function (_super) {
    __extends(PetserviceService, _super);
    function PetserviceService(defineHttp) {
        var _this = _super.call(this, defineHttp) || this;
        _this.http = defineHttp;
        _this.apiURL = 'http://localhost:5001/api/Pet/';
        return _this;
    }
    PetserviceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PetserviceService);
    return PetserviceService;
}(_Models_http_interface__WEBPACK_IMPORTED_MODULE_0__["HttpService"]));



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-form-field {\n  width: 100%;\n}\n\n.main {\n  margin: 0 auto;\n  padding: 15px;\n  max-width: 900px;\n}\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n    <a class=\"navbar-brand\" href=\"#\">Navbar</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n  \n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n      <ul class=\"navbar-nav mr-auto\">\n        <li class=\"nav-item active\">\n          <a class=\"nav-link\" routerLink=\"/pets\">Pets</a>\n        </li>\n        <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLink=\"/claims\">Claims</a>\n        </li>\n      </ul>\n    </div>\n  </nav>\n</div>\n<div class=\"w-100\">\n  <div>\n    <h2 class=\"m-3\">\n      <!-- PetSure -->\n    </h2>\n  </div>\n  <div class=\"w-75 mx-auto\">\n    <router-outlet>\n\n    </router-outlet>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _Services_petservice_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Services/petservice.service */ "./src/app/Services/petservice.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(_service) {
        this._service = _service;
        this._service.loadAll();
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_Services_petservice_service__WEBPACK_IMPORTED_MODULE_0__["PetserviceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Components_claims_claims_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Components/claims/claims.component */ "./src/app/Components/claims/claims.component.ts");
/* harmony import */ var _file_value_accessor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./file-value.accessor */ "./src/app/file-value.accessor.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _HttpsRequestInterceptor_HttpsRequestInterceptor_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./HttpsRequestInterceptor/HttpsRequestInterceptor.module */ "./src/app/HttpsRequestInterceptor/HttpsRequestInterceptor.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ngx-formly/core */ "./node_modules/@ngx-formly/core/fesm5/ngx-formly-core.js");
/* harmony import */ var _ngx_formly_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-formly/material */ "./node_modules/@ngx-formly/material/fesm5/ngx-formly-material.js");
/* harmony import */ var _Services_petservice_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Services/petservice.service */ "./src/app/Services/petservice.service.ts");
/* harmony import */ var _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../node_modules/@angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _Extensions_format_extensions__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./Extensions/format.extensions */ "./src/app/Extensions/format.extensions.ts");
/* harmony import */ var _repeat_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./repeat-component */ "./src/app/repeat-component.ts");
/* harmony import */ var _Components_pets_pets_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./Components/pets/pets.component */ "./src/app/Components/pets/pets.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _repeat_component__WEBPACK_IMPORTED_MODULE_16__["RepeatTypeComponent"],
                _file_value_accessor__WEBPACK_IMPORTED_MODULE_2__["FileValueAccessor"],
                _file_value_accessor__WEBPACK_IMPORTED_MODULE_2__["FormlyFieldFile"],
                _Components_claims_claims_component__WEBPACK_IMPORTED_MODULE_1__["ClaimsComponent"],
                _Components_pets_pets_component__WEBPACK_IMPORTED_MODULE_17__["PetsComponent"]
            ],
            imports: [
                _app_routing__WEBPACK_IMPORTED_MODULE_18__["routes"],
                _app_routing__WEBPACK_IMPORTED_MODULE_18__["appRoutes"],
                _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSliderModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
                _ngx_formly_core__WEBPACK_IMPORTED_MODULE_11__["FormlyModule"].forRoot({ types: [
                        { name: 'repeat', component: _repeat_component__WEBPACK_IMPORTED_MODULE_16__["RepeatTypeComponent"] },
                        { name: 'file', component: _file_value_accessor__WEBPACK_IMPORTED_MODULE_2__["FormlyFieldFile"] },
                    ],
                }),
                _ngx_formly_material__WEBPACK_IMPORTED_MODULE_12__["FormlyMaterialModule"],
                _node_modules_angular_common_http__WEBPACK_IMPORTED_MODULE_14__["HttpClientModule"],
                _HttpsRequestInterceptor_HttpsRequestInterceptor_module__WEBPACK_IMPORTED_MODULE_4__["InterceptorModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"]
            ],
            providers: [_Services_petservice_service__WEBPACK_IMPORTED_MODULE_13__["PetserviceService"],
                _Extensions_format_extensions__WEBPACK_IMPORTED_MODULE_15__["ExtensionService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes, appRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appRoutes", function() { return appRoutes; });
/* harmony import */ var _Components_pets_pets_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Components/pets/pets.component */ "./src/app/Components/pets/pets.component.ts");
/* harmony import */ var _Components_claims_claims_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Components/claims/claims.component */ "./src/app/Components/claims/claims.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var router = [
    { path: 'claims', component: _Components_claims_claims_component__WEBPACK_IMPORTED_MODULE_1__["ClaimsComponent"] },
    { path: 'pets', component: _Components_pets_pets_component__WEBPACK_IMPORTED_MODULE_0__["PetsComponent"] },
    { path: '**', component: _Components_claims_claims_component__WEBPACK_IMPORTED_MODULE_1__["ClaimsComponent"] }
];
var routes = _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(router);
var appRoutes = _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(router);


/***/ }),

/***/ "./src/app/file-value.accessor.ts":
/*!****************************************!*\
  !*** ./src/app/file-value.accessor.ts ***!
  \****************************************/
/*! exports provided: FileValueAccessor, FormlyFieldFile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileValueAccessor", function() { return FileValueAccessor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormlyFieldFile", function() { return FormlyFieldFile; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-formly/core */ "./node_modules/@ngx-formly/core/fesm5/ngx-formly-core.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FileValueAccessor = /** @class */ (function () {
    function FileValueAccessor() {
        this.onChange = function (_) { };
        this.onTouched = function () { };
    }
    FileValueAccessor_1 = FileValueAccessor;
    FileValueAccessor.prototype.writeValue = function (value) { };
    FileValueAccessor.prototype.registerOnChange = function (fn) { this.onChange = fn; };
    FileValueAccessor.prototype.registerOnTouched = function (fn) { this.onTouched = fn; };
    FileValueAccessor = FileValueAccessor_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            // tslint:disable-next-line
            selector: 'input[type=file]',
            // tslint:disable-next-line:use-host-property-decorator
            host: {
                '(change)': 'onChange($event.target.files)',
                '(blur)': 'onTouched()',
            },
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"], useExisting: FileValueAccessor_1, multi: true },
            ],
        })
        // https://github.com/angular/angular/issues/7341
        // tslint:disable-next-line:directive-class-suffix
    ], FileValueAccessor);
    return FileValueAccessor;
    var FileValueAccessor_1;
}());



var FormlyFieldFile = /** @class */ (function (_super) {
    __extends(FormlyFieldFile, _super);
    // tslint:disable-next-line:component-class-suffix
    function FormlyFieldFile() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldFile = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            // tslint:disable-next-line:component-selector
            selector: 'formly-field-file',
            template: "\n    <input type=\"file\" [formControl]=\"formControl\" [formlyAttributes]=\"field\">\n  ",
        })
        // tslint:disable-next-line:component-class-suffix
    ], FormlyFieldFile);
    return FormlyFieldFile;
}(_ngx_formly_core__WEBPACK_IMPORTED_MODULE_2__["FieldType"]));



/***/ }),

/***/ "./src/app/repeat-component.ts":
/*!*************************************!*\
  !*** ./src/app/repeat-component.ts ***!
  \*************************************/
/*! exports provided: RepeatTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepeatTypeComponent", function() { return RepeatTypeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngx-formly/core */ "./node_modules/@ngx-formly/core/fesm5/ngx-formly-core.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RepeatTypeComponent = /** @class */ (function (_super) {
    __extends(RepeatTypeComponent, _super);
    function RepeatTypeComponent(builder) {
        return _super.call(this, builder) || this;
    }
    RepeatTypeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            // tslint:disable-next-line:component-selector
            selector: 'formly-repeat-section',
            template: "\n    <div *ngFor=\"let field of field.fieldGroup; let i = index;\" class=\"row\">\n      <formly-field class=\"col\" [field]=\"field\"></formly-field>\n      <div class=\"col-sm-2 d-flex align-items-center\">\n        <button class=\"btn btn-danger\" type=\"button\" (click)=\"remove(i)\">Remove</button>\n      </div>\n    </div>\n    <div style=\"margin:30px 0;\">\n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"add()\"> Add a file</button>\n    </div>\n  ",
        }),
        __metadata("design:paramtypes", [_ngx_formly_core__WEBPACK_IMPORTED_MODULE_1__["FormlyFormBuilder"]])
    ], RepeatTypeComponent);
    return RepeatTypeComponent;
}(_ngx_formly_core__WEBPACK_IMPORTED_MODULE_1__["FieldArrayType"]));



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/Odale/Documents/pet_sure/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map